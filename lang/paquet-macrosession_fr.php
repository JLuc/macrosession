<?php
// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/switchcase/trunk/lang/

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'macrosession_nom' => 'Macros de session étendue',
	'macrosession_slogan' => 'Macros SPIP : #_SESSION, #_SESSION_SI, #_AUTORISER_SI etc pour accéder aux données de session SPIP ou d’autres origines, ainsi qu’aux autorisations et les tester dans un squelette sans multiplier les caches sessionnés.',
);
